var async = require('async');
var Series = require("../models/series");
var Article = require("../models/article");
var articleServices = require("./article");

module.exports = {
    create: (seriesData, articleData) => {
        return new Promise((resolve, reject) => {
            articleServices.createMulti(articleData).then((articles) =>{
                let series = new Series(seriesData);
                series.articles = articles;
                series.save((err) => {
                    if (err) {
                        reject(err);
                    }
                    articles.forEach((art) => {
                        let query = {'_id': art._id};
                        let updateData = {'series': series};
                        articleServices.update(query, updateData);
                    });
                    resolve({'success': true})
                });
            });                 
        });
    },

    update: (seriesData, articleData) => {
        return new Promise((resolve, reject) => {
            let seriesId = seriesData.id;

            let updateArticles = articleData.filter(function (el) {
                return el.id;
            });
            let createArticles = articleData.filter(function (el) {
                return !el.id;
            });

            createArticles.forEach(function(a) {
                a.series = seriesId;
            });
            
            Series.findOneAndUpdate({_id:seriesId}, seriesData, {upsert:true}).then((seriesRes) => {
                // Update Articles which already have ID
                if (updateArticles) {
                    async.eachSeries(updateArticles, function updateArt (uArt, done) {
                        Article.update({ _id: uArt.id }, uArt, {upsert:true}, done);
                    }, function allDone (err) {
                        if (err) reject(err);
                        if (!createArticles) resolve({'success': true })
                    });
                }

                if (createArticles) {
                    articleServices.createMulti(createArticles).then((cArts) => {
                        let cArtIds = cArts.map(a => a._id);
                        async.eachSeries(cArtIds, function updateArt(cArtId, done) {
                            Series.update({_id: seriesId}, {$push: {articles: cArtId}}, done);
                        }, function allDone (err) {
                            if (err) reject(err);
                            resolve({'success': true })
                        });
                    });
                }
            })
            .catch((error) => {
                reject(error);
            })
        });
    },

    search: (query={}, limit=10, offset=0) => {
        return new Promise((resolve, reject) => {
            if (! query.active) query.active = true;
            Series.find(query).skip(offset).limit(limit).populate("articles")
            .then((results) => {
                resolve(results);
            })
            .catch((error) => {
                reject(error);
            }) 
        });
    },

    remove: (data={}) => {
        return new Promise((resolve, reject) => {
            Series.findOneAndRemove(data)
            .then((results) => {
                let articleList = results.articles;
                Article.remove({ "_id": { "$in": articleList } })
                .then((results) => {
                    resolve(true);
                })
                .catch((error) => {
                    reject(error);
                }) 
                
            })
            .catch((error) => {
                reject(error);
            }) 
        });
    },

    count: (query={}) => {
        return new Promise((resolve, reject) => {
            Series.count(query)
            .then((count) => {
                resolve(count);
            })
            .catch((error) => {
                reject(error);
            }) 
        });
    },

    findById: (id) => {
        return new Promise((resolve, reject) => {
            Series.findOne({ _id: id, active: true }).populate("articles")
            .then((result) => {
                resolve(result);
            })
            .catch((error) => {
                reject(error);
            }) 
        });
    },
}
