var async = require('async');
var Article = require("../models/article");
var Series = require("../models/series");

module.exports = {
    create: (data) => {
        return new Promise((resolve, reject) => {
            Article.create(data, (err, result) => {
                if (err) callback(err);
                resolve(result);
            });
        });
    },

    update: (query, data) => {
        return new Promise((resolve, reject) => {
            Article.findOneAndUpdate(query, data, {upsert: true}, (err, result) => {
                if (err) callback(err);
                resolve(result);
            });
        });
    },

    search: (data={}, limit=80, offset=1) => {
        return new Promise((resolve, reject) => {
            if (! data.active) data.active = true;
            Article.find(data).skip(offset).limit(limit).populate("series")
            .then((results) => {
                resolve(results);
            })
            .catch((error) => {
                reject(error);
            }) 
        });
    },

    remove: (data={}) => {
        return new Promise((resolve, reject) => {
            Article.findOneAndRemove(data)
            .then((result) => {
                let articleId = result._id;
                let seriesId = result.series;
                Series.updateOne({"_id": seriesId}, {$pull: {articles: articleId } } )
                .then((results) => {
                    resolve(true);
                })
                .catch((error) => {
                    reject(error);
                }) 
            })
            .catch((error) => {
                reject(error);
            }) 
        });
    },

    findByUrl: (url) => {
        return new Promise((resolve, reject) => {
            Article.find({ url: { "$regex": url, "$options": "i" }, active: true }).populate("series")
            .then((results) => {
                let highestRatio = results.reduce((prev, current) => {
                    return (prev.series.ratio > current.series.ratio) ? prev : current
                });

                resolve(highestRatio);
            })
            .catch((error) => {
                reject(error);
            }) 
        });
    },

    createMulti: (datas) => {
        let newArts = [];
        datas.forEach(article => {
            let newArt = new Article(article);
            newArts.push(newArt.save((err) => {
                if (err) {
                    console.log(err);
                    reject(err);
                }
            }));
        });
        return Promise.all(newArts);
    },

    
}