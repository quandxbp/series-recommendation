var async = require('async');
var User = require("../models/user");
const uuidv1 = require('uuid/v1');
var constants = require('../common/constants');
var brandConstants = require('../common/brandConstants');
var keys = require('../config/keys');
var utils = require("../common/utils");

module.exports = {
    changeUserPassword: (userId, oldPassword, newPassword) => {
        return new Promise((resolve, reject) => {
            async.parallel([
                // check whether old password is correct
                (callback) => {
                    User.findOne({
                        _id: userId,
                        active: true
                    }, (err, foundUser) => {
                        if (err) {
                            callback(err);
                        } else {
                            if (!foundUser) {
                                reject("No user found!");
                            }
                            foundUser.authenticate(oldPassword, function (err, user, passwordError) {
                                if (err) {
                                    callback(err);
                                } else if (passwordError) {
                                    callback('The given password is incorrect.')
                                } else if (user) {
                                    console.log('correct password');
                                    callback(null, user);
                                }
                            });
                        }
                    });
                },
                (callback) => {
                    if (oldPassword == newPassword) {
                        callback('New password should not match old password.');
                    }
                    callback(null);
                }
            ], (err, results) => {
                if (err) {
                    reject(err);
                } else {
                    var user = results[0];
                    user.setPassword(newPassword, (err) => {
                        if (err) {
                            reject(err);
                        }
                        user.save();
                        console.log("User updated.");
                        resolve(user);
                    });
                }
            })
        });
    },

    validateForgotPassword: (url) => {
        return new Promise((resolve, reject) => {
            Url.findOne({ url, active: true }, (err, foundUrl) => {
                if (err) {
                    reject(constants.ERROR_WHEN_QUERY_DB);
                } else {
                    if (!foundUrl) {
                        reject(constants.NO_ENTRY_FOUND);
                    } else {
                        // after found it, set it to false, to ensure 1 user can reset password
                        foundUrl.active = false;
                        foundUrl.save();
                        resolve(foundUrl);
                    }
                }
            });
        });
    },

    // forgotPassword: (mail, domainName) => {
    //     return new Promise((resolve, reject) => {
    //         async.waterfall([
    //             (callback) => {
    //                 User.findOne({ mail: mail, active: true }).exec((err, user) => {
    //                     if (err) {
    //                         callback(constants.ERROR_WHEN_QUERY_DB);
    //                     } else {
    //                         console.log("user " + user);
    //                         if (!user) {
    //                             callback(constants.NO_ENTRY_FOUND);
    //                         } else {
    //                             callback(null, user);
    //                         }
    //                     }
    //                 });
    //             },
    //             (user, callback) => {
    //                 Url.count().exec((err, c) => {
    //                     if (err) {
    //                         callback(constants.ERROR_WHEN_QUERY_DB);
    //                     } else {
    //                         Url.create({
    //                             shortUrl: "/forgot_password_" + ++c,
    //                             url: domainName + "/reset-password/" + uuidv1() + "/" + Buffer.from(utils.appendAtStartAndEndOfWord(user._id, "P4ssWord")).toString('base64')
    //                         }, (err, url) => {
    //                             if (err) {
    //                                 callback(constants.ERROR_WHEN_INSERT_DB);
    //                             } else {
    //                                 console.log("Create url success.");
    //                                 callback(null, user, url)
    //                             }
    //                         });
    //                     }
    //                 });
    //             },
    //             (user, url, callback) => {
    //                 Landing.findOne({name: 'all-site'}, ['brandTitle'], (err, landing) => {
    //                     if(err) {
    //                         console.log(err);
    //                         callback(constant.ERROR_WHEN_QUERY_DB);
    //                     } else {
    //                         callback(null, user, url, landing.brandTitle);
    //                     }
    //                 });
    //             }
    //         ], (err, user, url, brandTitle) => {
    //             if (err) {
    //                 reject(err);
    //             } else {
    //                 setTimeout(function() {
    //                     Url.findByIdAndUpdate(url._id, { active: false }, (err, updatedUrl) => {
    //                         if (err) {
    //                             reject(err);
    //                         } else {
    //                             console.log("Url " + updatedUrl.shortUrl + " was set to unactived");
    //                         }
    //                     })
    //                 }, 300000); // 5 minutes after create url

    //                 var transporter = nodemailer.createTransport({
    //                     service: 'gmail',
    //                     auth: {
    //                         user: keys.MAIL_USERNAME,
    //                         pass: keys.MAIL_PASSWORD
    //                     }
    //                 });

    //                 var subject = `[` + brandTitle + `] Reset mật khẩu trang quản trị`;

    //                 var message = `
    //                 <p>Xin chào ` + user.fullName + `,</p>
    //                 <br>
    //                 <p>Chúng tôi đã nhận được yêu cầu đặt lại mật khẩu của bạn.</p>
    //                 <br>
    //                 <p>Vui lòng click vào <a href=` + domainName + url.shortUrl + `>link sau</a> để reset password.</p>
    //                 <br>
    //                 <p>Link expire sau 5 phút.</p>
    //                 `;

    //                 var mailOptions = {
    //                     from: keys.MAIL_USERNAME,
    //                     to: mail,
    //                     subject: subject,
    //                     html: message
    //                 };

    //                 transporter.sendMail(mailOptions, (error, info) => {
    //                     if (error) {
    //                         reject(constants.ERROR_WHEN_CALLING_EXTERNAL_SERVICE);
    //                     } else {
    //                         console.log(JSON.stringify(info));
    //                         resolve("Reset password mail was sent to " + info.accepted[0]);
    //                     }
    //                 });
    //             }
    //         });
    //     });
    // },

    resetPassword: (userId, newPasswordConfirmation) => {
        return new Promise((resolve, reject) => {
            User.findById(userId, (err, user) => {
                if (err) {
                    callback(constants.ERROR_WHEN_QUERY_DB);
                } else {
                    user.setPassword(newPasswordConfirmation, (err) => {
                        if (err) {
                            reject(err);
                        }
                        user.save();
                        console.log("User updated.");
                        resolve(user);
                    });
                }
            });
        });
    }
}