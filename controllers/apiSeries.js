const boom = require("boom");
const cloneDeep = require("lodash.clonedeep");
const mongoose = require("mongoose");

const constants = require("../common/constants"),
  to = require("../common/to"),
  Series = require("../models/series");

//middleware for create
module.exports = {
  getAll: async (req, res, next) => {
    try {
      let err, series;

      const query = Object.assign({}, req.query.q, {
        active: true,
      });

      [err, series] = await to(
        Series.find(query)
          .populate('articles', 'name title url feature_img ratio -_id')
          .sort({ dateCreated: -1 })
          .lean()
      );
      if (err) throw err;

      return res.json(series);
    } catch (error) {
      next(boom.internal(error));
    }
  },

  getDetail: async (req, res, next) => {
    return res.json(req.series);
  },

  getById: async (req, res, next, seriesId) => {
    try {
      let err, series;

      console.log("seriesId " + seriesId);

      [err, series] = await to(Series.findById(mongoose.Types.ObjectId(seriesId)));
      if (err) throw err;

      console.log("---------");
      console.log(JSON.stringify(series));

      if (!series) return next(boom.notFound("No object found."));

      req.series = series;

      next();
    } catch (error) {
      console.log(error);
      return next(boom.internal(error));
    }
  },
};
