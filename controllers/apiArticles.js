const boom = require("boom");
const cloneDeep = require("lodash.clonedeep");
const mongoose = require("mongoose");

const constants = require("../common/constants"),
    utils = require('../common/utils'),
    to = require("../common/to"),
    Article = require("../models/article");

//middleware for create
module.exports = {
    getAll: async (req, res, next) => {
        try {
            let err, articles;

            console.log("----------");
            console.log(JSON.stringify(req.query.q));
            console.log("----------");

            const query = Object.assign({}, req.query.q, {
                active: true,
            });

            [err, articles] = await to(
                Article.find(query)
                    .populate({
                        path: 'series',
                        select: 'name articles',
                        populate: {
                            path: 'articles',
                        }
                    })
                    .sort({ dateCreated: -1 })
                    .lean()
            );
            if (err) throw err;

            return res.json(articles);
        } catch (error) {
            next(boom.internal(error));
        }
    },

    getAllRecommend: async (req, res, next) => {
        try {
            let err, articles, resultArticles;

            const query = Object.assign({}, req.query.q, {
                active: true,
            });

            [err, articles] = await to(
                Article.find(query)
                    .populate({
                        path: 'series',
                        select: 'name articles',
                        match: { active: true },
                        populate: {
                            path: 'articles',
                            select: 'title url feature_img sequence'
                        }
                    })
                    .sort({ created: -1 })
                    .lean()
            );
            if (err) throw err;
            if (!articles) res.json([]);

            var rd = utils.getRandomInt(articles.length);

            // console.log("lenght: " + articles.length);
            // console.log("rd " + rd);

            // this is the result after finding the result article in series in list of articles
            resultArticles = articles[rd];

            // console.log("resultArticles " + JSON.stringify(resultArticles));

            // there case where series is null
            if (resultArticles.series) {
                // set the current query article to true in list
                resultArticles.series.articles[resultArticles.sequence - 1]['active'] = true;   
            }  else {
                resultArticles.series = [];
            }
            
            return res.json(resultArticles.series);
        } catch (error) {
            console.log(error);
            next(boom.internal(error));
        }
    },

    getDetail: async (req, res, next) => {
        return res.json(req.article);
    },

    getById: async (req, res, next, articleId) => {
        try {
            let err, article;

            [err, article] = await to(Article
                .findById(mongoose.Types.ObjectId(articleId))
                .populate('series', 'name')
            );
            if (err) throw err;

            if (!article) return next(boom.notFound("No object found."));

            req.article = article;

            next();
        } catch (error) {
            console.log(error);
            return next(boom.internal(error));
        }
    },
};
