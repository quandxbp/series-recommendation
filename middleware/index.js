var middlewareObj = {};

middlewareObj.isLoggedIn = function (req, res, next) {
  if (req.isAuthenticated() && req.user.active) {
    return next();
  } else {
    req.flash("error", "Please login!");
    return res.redirect("/login");
  }
};

middlewareObj.requireAdmin = function (req, res, next) {
  if (!req.user) {
    req.flash("error", "Please login!");
    return res.redirect("/login");
  } else if (req.isAuthenticated() && req.user.isAdmin) {
    return next();
  } else {
    req.flash("error", "you don't have authorization to view this page!");
    return res.redirect("/login");
  }
};

module.exports = middlewareObj;
