var express = require("express");
var router = express.Router();
var seriesService = require('../services/series');
var articleService = require('../services/article');
var constants = require('../common/constants');
const cheerio = require('cheerio');
const axios = require('axios')

// INDEX

router.get("/getByUrl", (req, res) => {
    let url = req.query.url;
    articleService.findByUrl(url).then((result) => {
        return res.send(result);
    }).catch(err => {
        return res.status(204).send(constants.NO_CONTENT);
    });
    
});

router.get('/search', (req, res) => {
    let limit = parseInt(req.query.limit);
    let offset = parseInt(req.query.offset);
    articleService.search({}, limit=limit, offset=offset).then((results) => {
        return res.send(results);
    }).catch(err => {
        return res.status(204).send(constants.NO_CONTENT);
    }); 
});

router.get('/crawl', (req, res) => {
    let url = req.query.url;
    axios.get(url).then((response) => {
        const $ = cheerio.load(response.data)
        let title = $('.entry-title').text();
        let featureImg = $('.featured-img img').attr('src');
        res.send({
            title: title,
            featureImg: featureImg,
        })
    }).catch(err => {
        return res.status(204).send(constants.NO_CONTENT);
    }); 
});


router.post('/unlink', (req, res) => {
    let id = req.body.recordId;
    articleService.remove({_id:id}).then((results) => {
        return res.send(results);
    }).catch(err => {
        return res.status(204).send(constants.NO_CONTENT);
    }); 
});

module.exports = router;