var express = require("express");
var router = express.Router();

const apiSeriesContr = require('../../controllers/apiSeries');

router.route(`/`)
    .get(apiSeriesContr.getAll);

router.route('/:seriesId')
    .get(apiSeriesContr.getDetail);

router.param('seriesId', apiSeriesContr.getById);

module.exports = router;