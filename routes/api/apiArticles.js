var express = require("express");
var router = express.Router();

const apiArticleContr = require('../../controllers/apiArticles');

router.route(`/`)
    .get(apiArticleContr.getAll);

router.route(`/recommend`)
    .get(apiArticleContr.getAllRecommend);

router.route('/:articleId')
    .get(apiArticleContr.getDetail);

router.param('articleId', apiArticleContr.getById);

module.exports = router;