var express = require("express");
var router = express.Router();
var constants = require('../common/constants');
var seriesService = require('../services/series');
var articleService = require('../services/article');

router.get("/getById", (req, res) => {
    let id = req.query.id;
    seriesService.findById(id).then((result) => {
        return res.send(result);
    }).catch(err => {
        return res.status(204).send(constants.NO_CONTENT);
    });
});

router.post("/create", (req, res) => {
    let series = req.body.series;
    let articles = req.body.articles;
    seriesService.create(series, articles).then((results) => {
        return res.send(results);
    }).catch(err => {
        return res.status(204).send(constants.NO_CONTENT);
    });
});

router.post("/update", (req, res) => {
    let series = req.body.series;
    let articles = req.body.articles;
    seriesService.update(series, articles).then((results) => {
        return res.send(results);
    }).catch(err => {
        return res.status(204).send(constants.NO_CONTENT);
    });
});

router.get('/search', (req, res) => {
    let query = req.query.query;
    let limit = parseInt(req.query.limit);
    let offset = parseInt(req.query.offset);
    seriesService.search(query, limit = limit, offset = offset).then((results) => {
        return res.send(results);
    }).catch(err => {
        return res.status(204).send(constants.NO_CONTENT);
    });
});

router.get('/count', (req, res) => {
    seriesService.count().then((count) => {
        return res.send({ count: count });
    }).catch(err => {
        console.log(err);
        return res.status(204).send(constants.NO_CONTENT);
    });
});

router.post('/unlink', (req, res) => {
    let id = req.body.recordId;
    seriesService.remove({ _id: id }).then((results) => {
        return res.send(results);
    }).catch(err => {
        return res.status(204).send(constants.NO_CONTENT);
    });
});



router.get("/create-series-sample-data", (req, res) => {
    let series_names = ["AAA", "BBB", "CCC", "DDD", "EEE", "FFF", "GGG", "HHH", "III", "JJJ"];
    let names = [
        ['SEO là gì', 'SEO Onpage', 'SEO Offpage', 'Lỗi kỹ thuật SEO', 'Khóa học SEO Fundamentals'],
        ['SEO là gì', 'Quy trình SEO', 'Hướng dẫn SEO web', 'Sai lầm SEO', 'Khóa học SEO Fundamentals'],
        ['SEO là gì', 'Học SEO có khó không', 'Hướng dẫn SEO web', 'Quy trình SEO', 'Khóa học SEO Fundamentals'],
        ['SEO là gì', 'Thuật ngữ SEO', 'Quảng cáo vs SEO', 'Chiến lược SEO', 'Khóa học SEO Fundamentals'],
        ['SEO là gì', 'Yếu tố quyết định thành công trong SEO', 'SEO mũ trắng', 'Cách tăng traffic', 'Khóa học SEO Fundamentals'],
        ['SEO là gì', 'Công cụ Ahrefs', 'Hướng dẫn sử dụng Ahrefs', 'Search intent', 'Khóa học SEO Fundamentals'],
        ['SEO là gì', 'Cách viết content hay', 'Cách viết bài chuẩn seo', 'Content ảnh hưởng đến SEO', 'Khóa học SEO Fundamentals'],
        ['SEO là gì', 'Khôi phục website sau update', 'Lỗi Kỹ thuật SEO', 'Hướng dẫn SEO web', 'Khóa học SEO Fundamentals'],
        ['SEO là gì', 'Kiểm tra website', 'Khôi phục website sau update', 'Thủ thuật SEO', 'Khóa học SEO Fundamentals'],
        ['SEO là gì', 'Content marketing', 'Từ khóa SEO', 'Nghiên cứu từ khóa', 'Khóa học SEO Fundamentals'],
    ];
    let urls = [
        ['https://gtvseo.com/seo-la-gi/', 'https://gtvseo.com/seo-onpage/', 'https://gtvseo.com/offpage-seo/', 'https://gtvseo.com/13-loi-ky-thuat-seo/', 'https://gtvseo.com/seo-fundamental/'],
        ['https://gtvseo.com/seo-la-gi/', 'https://gtvseo.com/quy-trinh-seo/', 'https://gtvseo.com/huong-dan-seo-web/', 'https://gtvseo.com/sai-lam-seo/', 'https://gtvseo.com/seo-fundamental/'],
        ['https://gtvseo.com/seo-la-gi/', 'https://gtvseo.com/hoc-seo-co-kho-khong/', 'https://gtvseo.com/huong-dan-seo-web/', 'https://gtvseo.com/quy-trinh-seo/', 'https://gtvseo.com/seo-fundamental/'],
        ['https://gtvseo.com/seo-la-gi/', 'https://gtvseo.com/thuat-ngu-seo/', 'https://gtvseo.com/quang-cao-seo/', 'https://gtvseo.com/chien-luoc-seo/', 'https://gtvseo.com/seo-fundamental/'],
        ['https://gtvseo.com/seo-la-gi/', 'https://gtvseo.com/3-yeu-quyet-dinh-thanh-cong-trong-seo/', 'https://gtvseo.com/seo-mu-trang/', 'https://gtvseo.com/cach-tang-traffic-cho-website/', 'https://gtvseo.com/seo-fundamental/'],
        ['https://gtvseo.com/seo-la-gi/', 'https://gtvseo.com/chi-so-ahrefs/', 'https://gtvseo.com/huong-dan-su-dung-ahrefs/', 'https://gtvseo.com/search-intent-la-gi/', 'https://gtvseo.com/seo-fundamental/'],
        ['https://gtvseo.com/seo-la-gi/', 'https://gtvseo.com/cach-viet-content-hay/', 'https://gtvseo.com/cach-viet-bai-chuan-seo/', 'https://gtvseo.com/content-anh-huong-toi-seo/', 'https://gtvseo.com/seo-fundamental/'],
        ['https://gtvseo.com/seo-la-gi/', 'https://gtvseo.com/khoi-phuc-website-sau-update/', 'https://gtvseo.com/13-loi-ky-thuat-seo/', 'https://gtvseo.com/huong-dan-seo-web/', 'https://gtvseo.com/seo-fundamental/'],
        ['https://gtvseo.com/seo-la-gi/', 'https://gtvseo.com/kiem-tra-website/', 'https://gtvseo.com/khoi-phuc-website-sau-update/', 'https://gtvseo.com/thu-thuat-seo-web/', 'https://gtvseo.com/seo-fundamental/'],
        ['https://gtvseo.com/seo-la-gi/', 'https://gtvseo.com/content-marketing-la-gi-1/', 'https://gtvseo.com/tu-khoa-seo/', 'https://gtvseo.com/nghien-cuu-tu-khoa/', 'https://gtvseo.com/seo-fundamental/'],
    ]

    for (let i = 0; i < 10; i++) {
        let series = { 'name': series_names[i] };
        let articles = []
        for (let j = 0; j < 5; j++) {
            let article = {
                name: names[i][j],
                url: urls[i][j],
                sequence: j + 1,
            }
            articles.push(article)
        }
        seriesService.create(series, articles);
    }
});


module.exports = router;