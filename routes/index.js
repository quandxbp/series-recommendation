var express = require("express");
var router = express.Router();
var passport = require("passport");
var userService = require('../services/user');
var utils = require("../common/utils");
var constants = require('../common/constants');

// INDEX
router.get("/", function (req, res) {
    return res.send('hello');
});

router.get("/seo-la-gi", function (req, res) {
    return res.render('articles/seo-la-gi');
});

router.get("/seo-onpage", function (req, res) {
    return res.render('articles/seo-onpage');
});

// ------------ LOGIN sections ------------
router.get("/login", function (req, res) {
    return res.render("login");
});

// LOGIN logic
router.post("/login", passport.authenticate("local",
    {
        successRedirect: "/cms",
        failureRedirect: "/login",
        failureFlash: true
    }), function (req, res) {
});

router.get("/logout", function (req, res) {
    req.logout();
    req.flash("success", "Logged you out!");
    return res.redirect("/login");
});

// Forgot password sections
router.get("/forgot-password", function (req, res) {
    return res.render("forgot-password");
});

router.post("/forgot-password", function (req, res) {
    userService.forgotPassword(req.body.mail, res.locals.domainName)
        .then(result => {
            req.flash("success", result);
            return res.redirect("/forgot-password");
        })
        .catch(error => {
            req.flash("error", error);
            return res.redirect("/forgot-password");
        });
});

module.exports = router;
