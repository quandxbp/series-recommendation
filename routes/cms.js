var express = require("express");
var router = express.Router();
var constants = require('../common/constants');
var middleware = require("../middleware/index");
var seriesService = require('../services/series');
var articleService = require('../services/article');

router.get("/", middleware.isLoggedIn, (req, res) => {
    return res.render("cms/landing");
});

router.get("/series", (req, res) => {
    return res.render("cms/series-management");
});

module.exports = router;