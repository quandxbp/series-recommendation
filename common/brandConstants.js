module.exports = {
    // Dao category id, Doi category id
    // since this is the two main category, we should set it here

    // Category id at 8084 (quatangcuocdoi.com)
    // DAO_1_CATEGORY_ID: '5c9c4ba16c89990304984100',
    // DOI_2_CATEGORY_ID: '5c9c4ba86c89990304984101',

    // Category id 8082 (dao-doi.com)
    DAO_1_CATEGORY_ID: '5c9c4b2fc4e9c007d0880634',
    DOI_2_CATEGORY_ID: '5c9c4b37c4e9c007d0880635',

    // Brand default information
    DEFAULT_BRAND_NAME: 'Đạo đời',
    DEFAULT_BRAND_META_DESCRIPTION: 'Chào mừng đến với website đạo đời.'
}