module.exports = {
  // query db mssage
  CREATE_SUCCESS: 'Create entry successfully.',
  UPDATE_SUCCESS: 'Update entry successfully.',
  DELETE_SUCCESS: 'Delete entry successfully.',

  // status code custom message
  METHOD_NOT_ALLOW: 'Method Not Allowed.',
  EMPTY_STRING_NOT_ALLOW: 'Empty string is not allow.',
  KEYWORD_NOT_ALLOW: 'Keyword is not allow.',
  ERROR_WHEN_QUERY_DB: 'Database query failed.',
  ERROR_WHEN_INSERT_DB: 'Database insert failed.',
  ERROR_WHEN_UPDATE_DB: 'Database update failed.',
  ERROR_WHEN_CALLING_EXTERNAL_SERVICE: 'Calling external service failed.',
  CALLING_EXTERNAL_SERVICE_SUCCESS: 'Calling service success.',
  NO_ENTRY_FOUND: 'No entry found.',
  SOMETHING_WRONG_HAPPENED: 'Something wrong happened.',

  // status message for draft post
  AUTO_SAVED_POST_INFO: 'New post will be saved automatically every 1 minutes.',
  SAVE_DRAFT_POST_SUCCESS: 'Auto-saved draft.',
  LOAD_DRAFT_POST_SUCCESS: 'Auto-loaded draft.',

  // Error code
  ERROR_CODE_QUERY_DUPLICATE_DB: 11000,

  // status code
  UNAUTHORIZED: 'Unauthorized.',
  NO_CONTENT: 'No content.',

  // video path for saving
  VIDEO_SAVE_PATH: "public/videos/",
  VIDEO_DOWNLOAD_PATH: "/videos/",

  // draft post format
  DRAFT_POST_NEW: 'draft-post-new',
  DRAFT_POST_EDIT: 'draft-post-edit',

  // model format
  POST_FORMAT: 1,
  BOOK_FORMAT: 2,
  VIDEO_FORMAT: 3,
  AUDIO_FORMAT: 4,
};  