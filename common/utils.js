var KhongDau = require('khong-dau');
var uuidv4 = require('uuid/v4');

module.exports = {
    chooseRandomString: () => {
        var randomNumber = Math.floor((Math.random() * 10) + 1); // from 1 to 10
        var result;
        switch (randomNumber) {
            case 1:
                result = 'Xem bài nguyên mẫu tại ';
                break;
            case 2:
                result = 'Tham khảo bài viết gốc ở';
                break;
            case 3:
                result = 'Tham khảo bài nguyên mẫu tại đây ';
                break;
            case 5:
                result = 'Coi thêm tại ';
                break;
            case 6:
                result = 'Coi thêm ở ';
                break;
            case 7:
                result = 'Đọc nguyên bài viết tại ';
                break;
            case 8:
                result = 'Coi nguyên bài viết ở ';
                break;
            case 9:
                result = 'Xem nguyên bài viết tại';
                break;
            case 10:
                result = 'Nguồn ';
                break;
            default:
                result = 'Coi bài nguyên văn tại ';
                break;
        }
        return result;
    },

    asyncForEach: async (array, callback) => {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    },

    getDate: () => {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        return yyyy + '/' + mm + '/' + dd;
    },

    optimizeUrl: (url) => {
        // url can't be new, since new is reserved keyword
        if (url == "new") {
            // auto generate url when reserved keyword
            return uuidv4();
        }
        url = url.trim();
        url = url.toLowerCase();
        url = url.replace(/[!@#$%^&*(),.?":{}|<>/]/g, "");
        url = KhongDau(url).split(' ').join('-');
        return url;
    },

    appendAtStartAndEndOfWord: (word, appendWord) => {
        var result = appendWord + "_" + word + "_" + appendWord;
        return result;
    },

    deleteAtStartAndEndOfWord: (word, deleteWord) => {
        var result = word.slice(deleteWord.length + 1); // delete at start _
        result = result.slice(0, result.length - (deleteWord.length + 1)); // delete at end _
        return result;
    },

    getRandomInt: (max) => {
        return Math.floor(Math.random() * Math.floor(max));
    }
}