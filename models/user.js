var mongoose = require("mongoose");
var passportLocalStrategy = require("passport-local-mongoose");

var userSchema = new mongoose.Schema({
    email: String,
    password: String,
    isAdmin: Boolean,
    fullName: String,
    description: String,
    image: String,
    created: { type: Date, default: Date.now },
    active: { type: Boolean, default: true }
});

userSchema.plugin(passportLocalStrategy, { usernameField: 'email' });

module.exports = mongoose.model("User", userSchema);
