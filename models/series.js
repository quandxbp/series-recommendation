let mongoose = require("mongoose");

let seriesSchema = new mongoose.Schema({
    name: String,
    sequence: { type: Number, min: 1},
    created: { type: Date, default: Date.now },
    modified: { type: Date, default: Date.now },
    active: { type: Boolean, default: true },
    ratio: { type: Number, min: 0, max: 100 , default: 0},
    articles: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Article"
        }
    ],
});

seriesSchema.pre('save', function(next) {
    this.modified = Date.now();
    next();
});

module.exports = mongoose.model("Series", seriesSchema);
