let mongoose = require("mongoose");

let articleSchema = new mongoose.Schema({
    name: String,
    title: String,
    url: String,
    feature_img: String,
    sequence: { type: Number, min: 1 },
    created: { type: Date, default: Date.now },
    modified: { type: Date, default: Date.now },
    ratio: { type: Number, min: 0, max: 100, default: 0 },
    series: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Series"
    },
    active: { type: Boolean, default: true }
});

articleSchema.pre('save', function (next) {
    this.modified = Date.now();
    next();
});

module.exports = mongoose.model("Article", articleSchema);
