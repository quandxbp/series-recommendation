$(document).ready(function() {
    getData(query={}, limit=10, page=1,  funcCall=renderSeriesTable);
    createSeriesEvent();
    updateEvent();
    removeEvent();
    paginationEvent();
    closeModalAction();
});

function renderSeriesTable(results) {
    let $SeriesTable = $('#seriesTable .series-content');
    $SeriesTable.empty();
    if (results) {
        let content = "";
        results.forEach(series => {
            let artNames = "";
            let artRatio = "";
            let artUrl = ""; 
            series.articles.forEach(art => {
                artNames += `<p>` + clean(art.name) + `</p>`;
                artUrl += `<p>` + clean(art.url) + `</p>`;
                artRatio += `<p>` + clean(art.ratio, true) + `</p>`;
            });

            content += `
                <tr class="series" data-series-id="` + series._id +`">
                    <td>` + clean(series.name) + ` </td>
                    <td>` + clean(series.ratio) + ` </td>
                    <td>` + clean(artNames) + `</td>
                    <td>` + clean(artUrl) + `</td>
                    <td>` + clean(artRatio, true) + `</td>
                    <td>
                        <i class="fa fa-pencil-alt hover-pointer edit-btn"></i>
                        <i class="fa fa-trash-alt hover-pointer remove-btn pl-2"></i>
                    </td>
                </tr>
            `;
        });
        $SeriesTable.append(content);
    } else {
        $SeriesTable.append(`<tr><td colspan=7>Không tìm thấy dữ liệu</td></tr>`);
    }
}

function renderArticleTable(articles) {
    let $ArticleTable = $('#seriesArticle .articles-content');
    $ArticleTable.empty();
    let content = "";
    articles.forEach(art => {
        content += `
            <tr class="articles" data-article-id="` + art._id +`">
                <td><input class="art-name" type="text" value="` + clean(art.name) + `" /></td>
                <td><input class="art-url" type="text" value="` + clean(art.url) + `" /></td>
                <td><input class="art-title" type="text" value="` + clean(art.title) + `" /></td>
                <td><input class="art-image-url" type="text" value="` + clean(art.feature_img) + `" /></td>
                <td><input class="max-width-50 art-ratio" type="number" min="0" value="` + clean(art.ratio, true) + `" /></td>
                <td>
                    <i class="fa fa-pencil-alt hover-pointer art-edit-btn"></i>
                    <i class="fa fa-download hover-pointer art-crawl-btn pl-2"></i>
                    <i class="fa fa-trash-alt hover-pointer art-remove-btn pl-2"></i>
                </td>
            </tr>
        `
    });
    $ArticleTable.append(content);
}

function renderUpdateForm(results) {
    clearForm();
    if (results) {
        res = results[0];
        $('#showUpdateForm').click();
        $('#seriesName').data('series-id', res._id);
        $('#seriesName').val(res.name);
        $('#seriesRatio').val(res.ratio);
        $('#seriesActive').prop('checked', res.active);
        renderArticleTable(res.articles);

        $('#seriesForm').on('click', '#changeSeries', function() {
            data = getSeriesFormData();
            updateSeries(data.series, data.articles);
        });
    }
}

function renderPagination(count, limit, page) {
    let $Pagination = $('.pagination');
    $Pagination.empty();
    page = parseInt(page);
    let lastPage = (count % limit) == 0 ? parseInt(count/limit) : parseInt(count/limit) + 1;
    let displayList = [page-2, page - 1, page, page + 1, page + 2]
    displayList.forEach(p => {
        if (p > 0 && p <= lastPage) {
            if (p == page) {
                $Pagination.append(`<li class="page-item active" data-page="` + p + `" ><a class="page-link" href="#">` + p + `</a></li>`)
            } else {
                $Pagination.append(`<li class="page-item" data-page="` + p + `" ><a class="page-link" href="#">` + p + `</a></li>`)
            }
        }
    });
}   

function getPagination(limit=10, page=1) {
    $.ajax({
        type: "GET",
        url: "../series/count",
        data: {},
        success: (data) => {
            renderPagination(count=data.count, limit=limit, page=page)
        },
        error: (err) => {
            console.log(err);
        }
    });
}

function getData(query={}, limit=10, page=false, funcCall) {
    let offset = 0;
    if (page) offset = (parseInt(page) - 1) * limit;
    $.ajax({
        type: "GET",
        url: "../series/search",
        data: {
            query: query,
            limit: limit,
            offset: offset,
        },
        success: (results) => {
            funcCall(results);
        },
        error: (err) => {
            console.log(err);
        }
    });

    if (page) getPagination(limit, page);
}

function unlinkSeries(recordId) {
    $.ajax({
        type: "POST",
        url: "../series/unlink",
        data: {
            recordId: recordId,
        },
        success: (results) => {
            getData(query={}, limit=10, page=1, funcCall=renderSeriesTable);
        },
        error: (err) => {
            alert("Gặp lỗi xóa Series");
        }
    });
}

function createSeries(series, articles) {
    $.ajax({
        type: "POST",
        url: "../series/create",
        data: {
            series: series,
            articles: articles,
        },
        success: (results) => {
            location.reload();
        },
        error: (err) => {
            alert("Gặp lỗi khi tạo Series");
        }
    });
}

function updateSeries(series, articles) {
    $.ajax({
        type: "POST",
        url: "../series/update",
        data: {
            series: series,
            articles: articles,
        },
        success: (results) => {
            // getData(query={}, limit=10, page=1, funcCall=renderSeriesTable);
            location.reload();
        },
        error: (err) => {
            alert("Gặp lỗi update Series");
        }
    });
}

function createSeriesEvent() {
    $('#createSeriesBtn').click(function() {
        clearForm();
        $("#submitSeries").removeClass('d-none');
    });
    
    $('#seriesForm').on('click', '#submitSeries', function() {
        data = getSeriesFormData();
        createSeries(data.series, data.articles);
    });
}

function updateEvent() {
    $('#seriesTable .series-content').on('click', '.edit-btn', function() {
        // Display "Change Series" button in form
        $("#changeSeries").removeClass('d-none');

        let $ParentSeries = $(this).parent().closest('tr');
        let seriesId = $ParentSeries.data('series-id');

        getData(query={_id: seriesId}, limit=1, page=false, funcCall=renderUpdateForm);
    });
}

function removeEvent() {
    $('#seriesTable .series-content').on('click', '.remove-btn', function() {
        if (confirm("Bạn có chắc chắn thực hiện thao tác này ?")) {
            let $ParentSeries = $(this).parent().closest('tr');
            let seriesId = $ParentSeries.data('series-id');
            unlinkSeries(recordId=seriesId);
        }
    });
}

function paginationEvent() {
    $('.pagination').on('click', '.page-item', function() {
        let page = $(this).data('page');
        getData(query={}, limit=10, page=page, funcCall=renderSeriesTable);
    });
}

function closeModalAction() {
    $('.close-modal').on('click', function() {
        $("#submitSeries").addClass('d-none');
        $("#changeSeries").addClass('d-none');
    });
}

function getSeriesFormData() {
    let isActive = false;
    if ($('#seriesForm #seriesActive').is(":checked")) isActive=true;

    let series = {
        id: $('#seriesForm #seriesName').data('series-id'),
        name: $('#seriesForm #seriesName').val(),
        ratio: $('#seriesForm #seriesRatio').val(),
        active: isActive,
    }

    let articles = [];
    let $ArticleContent = $('#seriesForm .articles-content');
    let sequence = $ArticleContent.children().length;

    let artIds = $ArticleContent.find('.articles');
    let names = $ArticleContent.find('.art-name');
    let titles = $ArticleContent.find('.art-title');
    let urls = $ArticleContent.find('.art-url');
    let imageUrls = $ArticleContent.find('.art-image-url');
    let ratios = $ArticleContent.find('.art-ratio');

    for (let i = 0; i < sequence; i++) {
        let article = {
            id: $(artIds[i]).data('article-id'),
            name: $(names[i]).val(),
            title: $(titles[i]).val(),
            url: $(urls[i]).val(),
            feature_img: $(imageUrls[i]).val(),
            ratio: $(ratios[i]).val(),
            sequence: i + 1,
        }
        articles.push(article)
    }
    return {
        series: series,
        articles: articles,
    }
}

function clean(value, number=false) {
    if (!value) {
        if (number) return 0;
        else return "";
    }
    return value;
}

function clearForm() {
    $('#seriesName').removeAttr('data-series-id');
    $('#seriesName').val('');
    $('#seriesRatio').val(0);
    $('.articles-content').empty();
}