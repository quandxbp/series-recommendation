const CONFIRM_PARAM = "confirm=yes";

// This Flag is to show series recommendation
var IS_SHOW_SERIES_RECOMMEND_BTN = true;
// This Flag is to show course recommendation
var IS_SHOW_COURSE_RECOMMEND_BTN = true;
// This Flag is to prevent checking params all the time
var CHECK_SCROLL = true; 
// var HASH_VALUE = window.location.hash.substr(1);
var CURRENT_URL = window.location.href

function showSeriesRecommendContent(){
    let pathname = window.location.pathname;

    $.ajax({
        type: "GET",
        url: "article/getByUrl",
        data: {
            url: pathname
        },
        success: (res) => {
            let $showSeriesRecommendBtn = $("#showSeriesRecommend");
            let $confirmNoSeriesBtn = $("#confirmNoSeriesBtn");
            $showSeriesRecommendBtn.fadeIn(1000);
            setValuesForSeriesBtn(res);
            
            $showSeriesRecommendBtn.click( () => {
                // Hide recommend button
                $showSeriesRecommendBtn.hide();
                showSeriesRecommendList(res.series._id);

                // Turn on SHOW SERIES flag
                IS_SHOW_SERIES_RECOMMEND_BTN = true;
                $confirmNoSeriesBtn.click( () => {
                    performSeriesActionNo(res.articles);
                });
            });
        },
        error: (err) => {
            console.log(err);
        }
    });
}

function showSeriesRecommendList(series_id) {
    $.ajax({
        type: "GET",
        url: "series/getById",
        data: {
            id: series_id
        },
        success: (res) => {
            setValuesForSeriesModal(res);
            // Show Recommend Modal
            let $seriesRecommendModal = $("#seriesRecommendModal");
            $seriesRecommendModal.fadeIn(1000);
        },
        error: (err) => {
            console.log(err);
        }
    });
}

function showCourseRecommendContent() {
    $("#courseRecommendModal").fadeIn(1000);
    $("#showCourseRecommend").click();
}

function setValuesForSeriesModal(data) {
    let seriesName = data.name;
    let articles = data.articles;

    let $SeriesRecommendList = $("#seriesRecommendModal .series-recommend-list");
    displayAricles(articles, $SeriesRecommendList);
}

function performSeriesActionNo(articles) {
    let $seriesComfirmNoModal = $("#seriesComfirmNoModal");
    $seriesComfirmNoModal.fadeIn(1000);

//     let $SeriesRecommendList = $("#seriesComfirmNoModal .series-recommend-list");
//     displayAricles(articles, $SeriesRecommendList);
}

function setValuesForSeriesBtn(data) {
    $('.articleSequence').text(data.sequence);
    $('.seriesName').text(data.series.name);
    $('.totalArts').text(data.series.articles.length);
}

function displayAricles(articles, $Pos) {
    $Pos.empty();
    articles.forEach((art) => {
        let href = art.url + "?" + CONFIRM_PARAM;
        $Pos.append(`
            <p><a href="` + href + `" target="_blank">` + art.sequence + `. `+ art.title + `</a></p>
        `)
    });
}

$(window).scroll((event) => {
    let scrollHeight = $(window).scrollTop();

    if (CHECK_SCROLL) {
        let url = new URL(CURRENT_URL);
        let confirmation = url.searchParams.get("confirm");

        // Case: User open url from recommendation
        if (confirmation && confirmation == "yes") {
            let halfPageHeight = $(document).height() / 2;
            // Show courses recommendation for users when scrolling to half of page
            if ((scrollHeight >= halfPageHeight) && IS_SHOW_COURSE_RECOMMEND_BTN) {
                showCourseRecommendContent()
                IS_SHOW_COURSE_RECOMMEND_BTN = false;
                CHECK_SCROLL = false;
            }
        } else {
            if ((scrollHeight >= 300) && IS_SHOW_SERIES_RECOMMEND_BTN) {
                showSeriesRecommendContent();
                IS_SHOW_SERIES_RECOMMEND_BTN = false;
                CHECK_SCROLL = false;
            }
        }
    }
});