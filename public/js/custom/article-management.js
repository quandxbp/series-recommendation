var SERIES_MODEL = 'series';
var ARTICLE_MODEL = 'article';

$(document).ready(function() {
    createArtEvent();
    crawlEvent();
    removeArtEvent();
});

function getCrawlData($ParentTr, artUrl) {
    $.ajax({
        type: "GET",
        url: "../article/crawl",
        data: {
            url: artUrl,
        },
        success: (res) => {
            $ParentTr.find('.art-title').val(res.title);
            $ParentTr.find('.art-image-url').val(res.featureImg);
        },
        error: (err) => {
            alert("Không tin thấy dữ liệu từ trang " + artUrl );
        }
    });
}

function unlinkArt(recordId, parent) {
    $.ajax({
        type: "POST",
        url: "../article/unlink",
        data: {
            recordId: recordId,
        },
        success: (results) => {
            parent.remove();
        },
        error: (err) => {
            alert("Gặp lỗi khi xóa Articles");
        }
    });
}

function createArtEvent() {
    $('#seriesForm').on('click', '#createArticlesBtn', function() {
        let $ArticleContent = $('#seriesForm .articles-content');
        let sequence = $ArticleContent.children().length + 1;
        $ArticleContent.append(
            `
                <tr>
                    <td><input class="art-name" type="text" /></td>
                    <td><input class="art-url" type="text" /></td>
                    <td><input class="art-title" type="text" /></td>
                    <td><input class="art-image-url" type="text" /></td>
                    <td><input class="max-width-50 art-ratio" type="number" min="0" value="0" /></td>
                    <td>
                        <i class="fa fa-pencil-alt hover-pointer art-edit-btn"></i>
                        <i class="fa fa-download hover-pointer art-crawl-btn pl-2"></i>
                        <i class="fa fa-trash-alt hover-pointer art-remove-btn pl-2"></i>
                    </td>
                </tr>
            `
        )
    });
}

function crawlEvent() {
    $('#seriesForm .articles-content').on('click', '.art-crawl-btn', function() {
        let $ParentTr = $(this).parent().closest('tr');
        let artUrlVal = $ParentTr.find('.art-url').val();
        if (checkEmpty([[artUrlVal, 'URL']])) {
            getCrawlData($ParentTr, artUrlVal);
        }
    });
}

function removeArtEvent() {
    $('#seriesForm .articles-content').on('click', '.art-remove-btn', function() {
        let $ParentTr = $(this).parent().closest('tr');
        let artId = $ParentTr.data('article-id');
        if (artId) {
            if (confirm("Bạn có chắc chắn thực hiện thao tác này ?")) {
                unlinkArt(recordId=artId, parent=$ParentTr);
            }
        } else {
            $ParentTr.remove();
        }
    
    });
}

function checkEmpty(checkList) {
    let warning = '';
    checkList.forEach(ele => {
        if (!ele[0]) {
            warning += ele[1] + " cần được bổ sung\n";
        }
    });

    if (warning != "") {
        alert(warning);
        return false
    }
    return true
}
