var express = require("express"),
  app = express(),
  bodyParser = require("body-parser"),
  mongoose = require("mongoose"),
  passport = require("passport"),
  LocalStrategy = require("passport-local"),
  methodOverride = require("method-override"),
  User = require("./models/user"),
  session = require('express-session'),
  flash = require("connect-flash"),
  morgan = require('morgan');
mongoose.Promise = require('bluebird');

// var seedDB = require("./seed");

var index = require("./routes/index"),
  seriesRoutes = require("./routes/series"),
  articleRoutes = require("./routes/article"),
  cmsRoutes = require("./routes/cms"),
  apiArticlesRoutes = require("./routes/api/apiArticles"),
  apiSeriesRoutes = require("./routes/api/apiSeries");

var keys = require('./config/keys');

// Connect the Mongo Mlab database
mongoose.connect(keys.DATABASEURL, { useMongoClient: true })
  .then((result) => {
    console.log("Connected to DB");
  })
  .catch(err => console.log(err));

// set up view engine .ejs
app.set("view engine", "ejs");

// expose the public folder
app.use(express.static("public"));

// parse the body
app.use(bodyParser.urlencoded({ extended: true }));

// for PUT request
app.use(methodOverride("_method"));

// use jquery
app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));

// use morgan to log requests to the console
app.use(morgan('dev'));

// for https
app.enable('trust proxy');

// required for passport session
app.use(session({
  secret: 'This is secret',
  saveUninitialized: false,
  resave: false
}));

// set up passport for authorization
app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// set up flash for user notification, display error for user
app.use(flash());

app.use(async (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");

  next();
});

app.use("/api/series/", apiSeriesRoutes)
app.use("/api/articles/", apiArticlesRoutes)

app.use("/series/", seriesRoutes)
app.use("/article/", articleRoutes)

// cms route
app.use("/cms/", cmsRoutes);

// index route
app.use("/", index);

app.get("*", (req, res) => {
  return res.status(404).send("404!");
});

// seedDB();

// error page handler
app.use((err, req, res, next) => {
  if (err.isServer) {
    // server error, should fix it fast
    console.log(err);
  }

  if (err.output != null && err.output.statusCode != null) {
    return res.status(err.output.statusCode).send(err.output.payload);
  } else {
    return res.status(500).send("Something wrong. " + err);
  }
});


app.listen(process.env.PORT || 8080, process.env.IP, function () {
  console.log("Starting the server! at PORT " + (process.env.PORT || 8080) + " and IP " + process.env.IP);
});
